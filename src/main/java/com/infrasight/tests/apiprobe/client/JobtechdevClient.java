package com.infrasight.tests.apiprobe.client;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.gson.GsonFactory;
import com.infrasight.tests.apiprobe.client.url.CitiesUrl;
import com.infrasight.tests.apiprobe.client.url.OfficeUrl;
import com.infrasight.tests.apiprobe.client.url.OfficesUrl;
import com.infrasight.tests.apiprobe.model.City;
import com.infrasight.tests.apiprobe.model.County;
import com.infrasight.tests.apiprobe.model.Office;
import com.infrasight.tests.apiprobe.model.search.GenericSearch;
import com.infrasight.tests.apiprobe.model.search.office.DetailedOffice;
import com.infrasight.tests.apiprobe.model.search.office.OfficeSearch;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Singleton class with methods to call on the services provided by Arbetsförmedlingens Jobtechdev services.
 */
final class JobtechdevClient {

    private JobtechdevClient() {
    }

    static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static JsonFactory JSON_FACTORY = new GsonFactory();
    static HttpRequestFactory HTTP_REQUEST_FACTORY;

    private static JobtechdevClient ourInstance = new JobtechdevClient();

    static {
        HTTP_REQUEST_FACTORY = HTTP_TRANSPORT.createRequestFactory(
                (HttpRequest request) -> {
                    request.setParser(new JsonObjectParser(JSON_FACTORY));
                    request.getHeaders()
                            .setAccept("application/json;charset=utf-8; qs=1")
                            .set("Accept-Language", "sv-SE");
                });
    }

    static JobtechdevClient getInstance() {
        return ourInstance;
    }

    List<County> getCounties() throws IOException {
        return buildGetRequest(new GenericUrl(Service.GET_ALL_COUNTIES.getUrl()))
                .execute()
                .parseAs(GenericSearch.class)
                .getGenericSearchResult()
                .getGenericSearchResultData()
                .stream()
                .map(data -> {
                    return new County(data.getId(), data.getName(), data.getNumJobAdverts());
                })
                .collect(Collectors.toList());
    }

    /**
     * @param countyId County ID.
     * @return List with offices for the county with the given ID.
     */
    List<Office> getOffices(String countyId) throws IOException {
        return buildGetRequest(new OfficesUrl(countyId))
                .execute()
                .parseAs(OfficeSearch.class)
                .getOfficeSearchResult()
                .getOffices();
    }

    Office getOffice(String officeCode) throws IOException {
        return buildGetRequest(new OfficeUrl(officeCode))
                .execute()
                .parseAs(DetailedOffice.class)
                .getOffice();
    }

    Stream<City> getCities(String countyId) throws IOException {
        return buildGetRequest(new CitiesUrl(countyId))
                .execute()
                .parseAs(GenericSearch.class)
                .getGenericSearchResult()
                .getGenericSearchResultData()
                .stream()
                .map(data -> {
                    return new City(data.getId(), data.getName(), data.getNumJobAdverts());
                });
    }

    private static HttpRequest buildGetRequest(GenericUrl url) throws IOException {
        return HTTP_REQUEST_FACTORY.buildGetRequest(url);
    }

}
