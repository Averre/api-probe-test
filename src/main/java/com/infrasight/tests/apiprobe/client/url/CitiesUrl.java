package com.infrasight.tests.apiprobe.client.url;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.util.Key;
import com.infrasight.tests.apiprobe.client.Service;

/**
 * Builds an Url to get all Cities for a County with the given ID.
 */
public class CitiesUrl extends GenericUrl {

    @Key("lanid")
    private String countyId;

    public CitiesUrl(String countyId) {
        super(Service.GET_ALL_CITIES.getUrl());
        this.countyId = countyId;
    }
}
