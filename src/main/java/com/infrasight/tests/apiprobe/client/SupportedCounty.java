package com.infrasight.tests.apiprobe.client;

public enum SupportedCounty {
    SCANIA("12");

    private final String id;

    SupportedCounty(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
