package com.infrasight.tests.apiprobe.client.url;

import com.google.api.client.http.GenericUrl;
import com.infrasight.tests.apiprobe.client.Service;

/**
 * Builds an Url to get the details for the Office with the given office code.
 */
public class OfficeUrl extends GenericUrl {

    public OfficeUrl(final String officeCode) {
        super(Service.GET_OFFICE.getUrl() + officeCode);
    }
}
