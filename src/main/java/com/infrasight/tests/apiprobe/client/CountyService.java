package com.infrasight.tests.apiprobe.client;

import com.infrasight.tests.apiprobe.model.City;
import com.infrasight.tests.apiprobe.model.County;

import java.io.IOException;
import java.util.stream.Collectors;

/**
 * Contains static methods to retrieve data for the custom {@link County} model class.
 */
public class CountyService {

    private CountyService() {
    }

    public static County getCounty(SupportedCounty supportedCounty) throws IOException {
        County county = JobtechdevClient.getInstance().getCounties()
                .stream()
                .filter(c -> c.getId().equals(supportedCounty.getId()))
                .findFirst()
                .get();

        return populateAdditionalCountyData(county);
    }

    private static County populateAdditionalCountyData(County county) throws IOException {
        if (county == null) {
            throw new IllegalArgumentException("Received null instead of a county instance.");
        }

        county.setOffices(
                JobtechdevClient.getInstance().getOffices(county.getId()).stream()
                        .map(office -> {
                            try {
                                return JobtechdevClient.getInstance().getOffice(office.getOfficeCode());
                            } catch (IOException ioe) {
                                throw new RuntimeException(ioe);
                            }
                        })
                        .collect(Collectors.toList())
        );

        county.setJobAdvertsPerCity(
                JobtechdevClient.getInstance().getCities(county.getId())
                        .collect(Collectors.toMap(City::getName, City::getNumJobAdverts))
        );

        return county;
    }
}
