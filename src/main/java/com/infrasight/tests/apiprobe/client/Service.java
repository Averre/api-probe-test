package com.infrasight.tests.apiprobe.client;

/**
 * Elements with Urls' to the services provided by Arbetsförmedlingen.
 */
public enum Service {
    GET_ALL_COUNTIES("https://api.arbetsformedlingen.se/af/v0/arbetsformedling/soklista/lan"),
    GET_ALL_OFFICES("https://api.arbetsformedlingen.se/af/v0/arbetsformedling/platser"),
    GET_ALL_CITIES("https://api.arbetsformedlingen.se/af/v0/platsannonser/soklista/kommuner"),
    GET_OFFICE("https://api.arbetsformedlingen.se/af/v0/arbetsformedling/");

    private final String url;

    Service(final String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
