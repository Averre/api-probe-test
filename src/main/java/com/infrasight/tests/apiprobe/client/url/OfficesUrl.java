package com.infrasight.tests.apiprobe.client.url;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.util.Key;
import com.infrasight.tests.apiprobe.client.Service;

/**
 * Builds an Url to get all Offices for a County with the given ID.
 */
public class OfficesUrl extends GenericUrl {

    @Key("lanid")
    private String countyId;

    public OfficesUrl(String countyId) {
        super(Service.GET_ALL_OFFICES.getUrl());
        this.countyId = countyId;
    }
}
