package com.infrasight.tests.apiprobe.model.search.office;

import com.google.api.client.util.Key;
import com.infrasight.tests.apiprobe.model.Office;

import java.util.List;

public class OfficeSearchResult {

    @Key("arbetsformedlingplatsdata")
    private List<Office> offices;

    public List<Office> getOffices() {
        return offices;
    }
}
