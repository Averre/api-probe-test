package com.infrasight.tests.apiprobe.model.search;

import com.google.api.client.util.Key;

public class GenericSearchResultData {

    /**
     * ID
     */
    @Key
    private String id;

    /**
     * Name
     */
    @Key("namn")
    private String name;

    /**
     * Number of job adverts in the county
     */
    @Key("antal_platsannonser")
    private int numJobAdverts;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNumJobAdverts() {
        return numJobAdverts;
    }
}
