package com.infrasight.tests.apiprobe.model.search.office;

import com.google.api.client.util.Key;

/**
 * JSON root when parsing response from {@link com.infrasight.tests.apiprobe.client.Service#GET_ALL_OFFICES}.
 */
public class OfficeSearch {

    @Key("arbetsformedlingslista")
    private OfficeSearchResult officeSearchResult;

    public OfficeSearchResult getOfficeSearchResult() {
        return officeSearchResult;
    }
}
