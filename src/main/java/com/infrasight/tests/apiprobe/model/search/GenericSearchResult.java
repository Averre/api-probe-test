package com.infrasight.tests.apiprobe.model.search;

import com.google.api.client.util.Key;

import java.util.List;

public class GenericSearchResult {

    @Key("sokdata")
    private List<GenericSearchResultData> genericSearchResultData;

    public List<GenericSearchResultData> getGenericSearchResultData() {
        return genericSearchResultData;
    }
}
