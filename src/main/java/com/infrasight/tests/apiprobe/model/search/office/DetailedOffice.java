package com.infrasight.tests.apiprobe.model.search.office;

import com.google.api.client.util.Key;
import com.infrasight.tests.apiprobe.model.Office;

/**
 * JSON root when parsing response from {@link com.infrasight.tests.apiprobe.client.Service#GET_OFFICE}.
 */
public class DetailedOffice {

    @Key("arbetsformedling")
    private Office office;

    public Office getOffice() {
        return office;
    }
}
