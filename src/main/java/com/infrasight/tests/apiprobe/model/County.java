package com.infrasight.tests.apiprobe.model;

import com.google.api.client.util.Key;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

/**
 * Represents a Swedish county
 */
public class County {

    /**
     * ID
     */
    @Key
    private String id;

    /**
     * Name
     */
    @Key("namn")
    private String name;

    /**
     * Number of job adverts in the county
     */
    @Key("antal_platsannonser")
    private int numJobAdverts;

    /**
     * Offices in the county (platser)
     */
    private List<Office> offices;

    /**
     * Number of job adverts per city/municipality
     */
    private Map<String, Integer> jobAdvertsPerCity;

    public County() {
    }

    public County(String id, String name, int numJobAdverts) {
        this.id = id;
        this.name = name;
        this.numJobAdverts = numJobAdverts;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNumJobAdverts() {
        return numJobAdverts;
    }

    public List<Office> getOffices() {
        return offices;
    }

    public void setOffices(List<Office> offices) {
        this.offices = offices;
    }

    public Map<String, Integer> getJobAdvertsPerCity() {
        return jobAdvertsPerCity;
    }

    public void setJobAdvertsPerCity(Map<String, Integer> jobAdvertsPerCity) {
        this.jobAdvertsPerCity = jobAdvertsPerCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        County county = (County) o;
        return Objects.equals(id, county.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String toString() {
        String ret = id + " (" + name + ")\n";
        ret += "Number of job adverts: " + numJobAdverts + "\n";

        ret += "Offices:\n";
        if (offices == null || offices.isEmpty())
            ret += "No offices found";
        else {
            for (Office k : offices) {
                ret += "  " + k + "\n";
            }
        }

        ret += "Job adverts per city:\n";
        if (jobAdvertsPerCity == null || jobAdvertsPerCity.isEmpty())
            ret += "No job adverts found";
        else {
            for (Entry<String, Integer> entry : jobAdvertsPerCity.entrySet()) {
                ret += "  " + entry.getKey() + ": " + entry.getValue() + "\n";
            }
        }
        return ret;
    }
}

