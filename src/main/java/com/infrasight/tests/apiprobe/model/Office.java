package com.infrasight.tests.apiprobe.model;

import com.google.api.client.util.Key;

import java.util.Objects;

/**
 * Represents an office (plats) for Arbetsformedligen
 */
public class Office {
    /**
     * Office Code (platskod)
     */
    @Key("afplatskod")
    private String officeCode;

    /**
     * Office name (platsnamn)
     */
    @Key("afplatsnamn")
    private String officeName;

    /**
     * Contact e-mail
     */
    @Key("epostadress")
    private String email;

    public String getOfficeCode() {
        return officeCode;
    }

    public String getOfficeName() {
        return officeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Office office = (Office) o;
        return Objects.equals(officeCode, office.officeCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(officeCode);
    }

    public String toString() {
        return officeName + " (" + officeCode + "): " + email;
    }
}
