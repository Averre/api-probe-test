package com.infrasight.tests.apiprobe.model;

import com.google.api.client.util.Key;

import java.util.Objects;

/**
 * Represents a Swedish city
 */
public class City {

    /**
     * ID
     */
    @Key
    private String id;

    /**
     * Name
     */
    @Key("namn")
    private String name;

    /**
     * Number of job adverts in the city
     */
    @Key("antal_platsannonser")
    private int numJobAdverts;

    public City() {
    }

    public City(String id, String name, int numJobAdverts) {
        this.id = id;
        this.name = name;
        this.numJobAdverts = numJobAdverts;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNumJobAdverts() {
        return numJobAdverts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(id, city.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
