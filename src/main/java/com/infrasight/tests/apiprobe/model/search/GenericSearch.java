package com.infrasight.tests.apiprobe.model.search;

import com.google.api.client.util.Key;

/**
 * JSON root when parsing response from {@link com.infrasight.tests.apiprobe.client.Service#GET_ALL_COUNTIES}
 * and {@link com.infrasight.tests.apiprobe.client.Service#GET_ALL_CITIES}.
 */
public class GenericSearch {

    @Key("soklista")
    private GenericSearchResult genericSearchResult;

    public GenericSearchResult getGenericSearchResult() {
        return genericSearchResult;
    }
}
