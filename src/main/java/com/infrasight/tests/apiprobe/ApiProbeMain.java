package com.infrasight.tests.apiprobe;

import com.infrasight.tests.apiprobe.client.CountyService;
import com.infrasight.tests.apiprobe.client.SupportedCounty;
import com.infrasight.tests.apiprobe.model.County;

import java.io.IOException;

public class ApiProbeMain {

    /**
     * Arbetsformedligen REST API documentation: https://jobtechdev.se/swagger
     * <p>
     * The County and Office Java classes have several fields that need data.
     * The task at hand is to use the API to fill in the fields for Skane lan (county).
     * You will need to use several of the API endpoints to fill all fields.
     * <p>
     * You may retrieve the data and populate the classes in any way you like.
     * <p>
     * Use any open source libraries you like to complete the task.
     * For example, there are several libraries for JSON processing:
     * Jackson, Google Gson, Org.JSON.
     * <p>
     * It is recommended to use Maven for library dependencies.
     * <p>
     * As a final step, print out the County object with the fields filled in.
     */
    public static void main(String[] args) {
        // TODO: Use the Arbetsformedligen API and create a new County object for Skane lan
        try {
            County scaniaCounty = CountyService.getCounty(SupportedCounty.SCANIA);

            // Print out information
            if (scaniaCounty == null)
                System.out.println("Scania county is null, please create the object and fill in some fields");
            else
                System.out.println(scaniaCounty);
        } catch (IOException ioe) {
            System.err.println("Request to Jobtechdev service failed!");
            System.err.println(ioe.getMessage());
        }
    }
}
