package com.infrasight.tests.apiprobe.client;

import com.google.api.client.http.*;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.testing.http.MockHttpTransport;
import com.google.api.client.testing.http.MockLowLevelHttpRequest;
import com.google.api.client.testing.http.MockLowLevelHttpResponse;

import java.io.IOException;

abstract class JobtechdevClientTest {

    protected void setupMockJobtechdevClient(final String testJsonData) {
        HttpTransport mockTransport = new MockHttpTransport() {
            @Override
            public LowLevelHttpRequest buildRequest(String method, String url) throws IOException {
                return new MockLowLevelHttpRequest() {
                    @Override
                    public LowLevelHttpResponse execute() throws IOException {
                        MockLowLevelHttpResponse result = new MockLowLevelHttpResponse();
                        result.setContentType(Json.MEDIA_TYPE);
                        result.setContent(testJsonData);
                        return result;
                    }
                };
            }
        };

        JsonFactory jsonFactory = new GsonFactory();

        HttpRequestFactory mockHttpRequestFactory = mockTransport.createRequestFactory(
                (HttpRequest request) -> {
                    request.setParser(new JsonObjectParser(jsonFactory));
                });

        JobtechdevClient.HTTP_TRANSPORT = mockTransport;
        JobtechdevClient.JSON_FACTORY = jsonFactory;
        JobtechdevClient.HTTP_REQUEST_FACTORY = mockHttpRequestFactory;
    }
}
