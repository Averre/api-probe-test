package com.infrasight.tests.apiprobe.client;

import com.infrasight.tests.apiprobe.model.City;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

public class CitiesSearchTest extends JobtechdevClientTest {

    @Test
    public void testCountyJsonParsing() throws Exception {
        final String countyId = "12";
        final String cityId = "1260";
        final String cityName = "Bjuv";
        final int cityNumJobAdverts = 38;
        final String testJson = createTestJson(cityId, cityName, cityNumJobAdverts);
        setupMockJobtechdevClient(testJson);

        List<City> cities = JobtechdevClient.getInstance().getCities(countyId).collect(Collectors.toList());

        Assertions.assertNotNull(cities);
        Assertions.assertFalse(cities.isEmpty());
        Assertions.assertEquals(1, cities.size());

        City city = cities.get(0);

        Assertions.assertEquals(cityId, city.getId());
        Assertions.assertEquals(cityName, city.getName());
        Assertions.assertEquals(cityNumJobAdverts, city.getNumJobAdverts());
    }

    private String createTestJson(final String id, final String name, final int numJobAdverts) {
        return "{\n" +
                "  \"soklista\": {\n" +
                "    \"listnamn\": \"kommuner\",\n" +
                "    \"totalt_antal_platsannonser\": 4981,\n" +
                "    \"totalt_antal_ledigajobb\": 9934,\n" +
                "    \"sokdata\": [\n" +
                "      {\n" +
                "        \"id\": \"" + id + "\",\n" +
                "        \"namn\": \"" + name + "\",\n" +
                "        \"antal_platsannonser\": " + numJobAdverts + ",\n" +
                "        \"antal_ledigajobb\": 45\n" +
                "      }\n" +
                "      ]\n" +
                "  }\n" +
                "}";
    }
}
