package com.infrasight.tests.apiprobe.client;

import com.infrasight.tests.apiprobe.model.County;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CountiesSearchTest extends JobtechdevClientTest {

    @Test
    public void testCountyJsonParsing() throws Exception {
        final String countyId = "10";
        final String countyName = "Blekinge län";
        final int countyNumJobAdverts = 421;
        final String testJson = createTestJson(countyId, countyName, countyNumJobAdverts);
        setupMockJobtechdevClient(testJson);

        List<County> counties = JobtechdevClient.getInstance().getCounties();

        Assertions.assertNotNull(counties);
        Assertions.assertFalse(counties.isEmpty());
        Assertions.assertEquals(1, counties.size());

        County county = counties.get(0);

        Assertions.assertEquals(countyId, county.getId());
        Assertions.assertEquals(countyName, county.getName());
        Assertions.assertEquals(countyNumJobAdverts, county.getNumJobAdverts());
    }

    private String createTestJson(final String id, final String name, final int numJobAdverts) {
        return "{\n" +
                "  \"soklista\": {\n" +
                "    \"listnamn\": \"lan\",\n" +
                "    \"totalt_antal_platsannonser\": 41798,\n" +
                "    \"totalt_antal_ledigajobb\": 94791,\n" +
                "    \"sokdata\": [\n" +
                "      {\n" +
                "        \"id\": \"" + id + "\",\n" +
                "        \"namn\": \"" + name + "\",\n" +
                "        \"antal_platsannonser\": " + numJobAdverts + ",\n" +
                "        \"antal_ledigajobb\": 744\n" +
                "      }\n" +
                "    ]\n" +
                "   }\n" +
                "}";
    }
}
