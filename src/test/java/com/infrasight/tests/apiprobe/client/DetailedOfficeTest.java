package com.infrasight.tests.apiprobe.client;

import com.infrasight.tests.apiprobe.model.Office;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DetailedOfficeTest extends JobtechdevClientTest {

    @Test
    public void testCountyJsonParsing() throws Exception {
        final String officeCode = "0120100";
        final String officeName = "Arbetsförmedlingen Arlöv";
        final String email = "lund@arbetsformedlingen.se";
        final String testJson = createTestJson(officeCode, officeName, email);
        setupMockJobtechdevClient(testJson);

        Office office = JobtechdevClient.getInstance().getOffice(officeCode);
        Assertions.assertNotNull(office);
        Assertions.assertEquals(officeCode, office.getOfficeCode());
        Assertions.assertEquals(officeName, office.getOfficeName());
        Assertions.assertEquals(email, office.getEmail());
    }

    String createTestJson(final String officeCode, final String officeName, final String email) {
        return "{\n" +
                "  \"arbetsformedling\": {\n" +
                "    \"afplatskod\": \"" + officeCode + "\",\n" +
                "    \"afplatsnamn\": \"" + officeName + "\",\n" +
                "    \"afplatschef\": \"Carolina Gianola\",\n" +
                "    \"besoksadress\": \"Företagsvägen 30 C\",\n" +
                "    \"besoksort\": \"Arlöv\",\n" +
                "    \"postnummer\": \"232 37\",\n" +
                "    \"postort\": \"Arlöv\",\n" +
                "    \"epostadress\": \"" + email + "\",\n" +
                "    \"kontakttelefonnummer\": \"0771600000\",\n" +
                "    \"arbetsgivartelefonnummer\": \"0104872161\",\n" +
                "    \"faxnummer\": \"0104860100\",\n" +
                "    \"telefontid\": \"Växeln är öppen måndag-fredag 8-16\",\n" +
                "    \"personligservicetid\": \"Endast bokade besök\",\n" +
                "    \"extra_information\": \"<html xmlns=\\\"http://www.w3.org/1999/xhtml/\\\"><head><title>Extrainfo</title><meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" /><meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\\\" /></head><body><span></span><h2 id=\\\"h-Foretagsradgivningmojlighetervierbjuder\\\">Företagsrådgivning, möjligheter vi erbjuder</h2><p>Vad kan vi göra för dig som arbetsgivare? Vi har olika kontaktvägar, välj det som passar dig. Ring oss på 010-487 21 61 eller <a title=\\\"\\\" href=\\\"mailto:lundaregionen@arbetsformedlingen.se\\\">mejla</a>.</p><h3 id=\\\"h-svdefaultanchor\\\"><br /></h3><h3 id=\\\"h-svdefaultanchor-0\\\"><strong></strong></h3><p><br /></p></body></html>\"\n" +
                "  }\n" +
                "}";
    }
}
