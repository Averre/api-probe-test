package com.infrasight.tests.apiprobe.client;

import com.infrasight.tests.apiprobe.model.Office;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class OfficesSearchTest extends JobtechdevClientTest {

    @Test
    public void testOfficesJsonParsing() throws Exception {
        final String countyId = "12";
        final String officeCode = "0120100";
        final String officeName = "Arlöv";
        final String testJson = createTestJson(officeCode, officeName);
        setupMockJobtechdevClient(testJson);

        List<Office> offices = JobtechdevClient.getInstance().getOffices(countyId);

        Assertions.assertNotNull(offices);
        Assertions.assertFalse(offices.isEmpty());
        Assertions.assertEquals(1, offices.size());

        Office office = offices.get(0);

        Assertions.assertEquals(officeCode, office.getOfficeCode());
        Assertions.assertEquals(officeName, office.getOfficeName());
    }

    String createTestJson(final String officeCode, final String officeName) {
        return "{\n" +
                "  \"arbetsformedlingslista\": {\n" +
                "    \"arbetsformedlingplatsdata\": [\n" +
                "      {\n" +
                "        \"afplatskod\": \"" + officeCode + "\",\n" +
                "        \"afplatsnamn\": \"" + officeName + "\"\n" +
                "      }\n" +
                "      ]\n" +
                "  }\n" +
                "}";
    }
}
